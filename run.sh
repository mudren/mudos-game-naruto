#!/bin/bash

# 定义变量
DRIVER_FILE="fluffos/bin/driver"
CONFIG_FILE="v2017.cfg"

# 判断 driver 文件是否存在
if [ -f "$DRIVER_FILE" ]; then
    # 如果 driver 文件存在，则运行 driver config.cfg
    "$DRIVER_FILE" "$CONFIG_FILE"
else
    # 如果 driver 文件不存在，则提示请先运行 build.sh 编译 driver
    echo "请先运行 build.sh 编译 driver。"
fi
