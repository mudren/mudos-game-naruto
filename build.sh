#!/bin/bash

# 如果 fluffos 目录不存在，则从 gitee 克隆 fluffos 仓库
if [ ! -d "fluffos" ]; then
    git clone https://gitee.com/mudren/fluffos.git
fi

# 切换到 fluffos v2017 分支
cd fluffos && git checkout v2017

# 进入项目源码目录
cd src

# 记录开始时间
starttime=`date +'%Y-%m-%d %H:%M:%S'`

# 检查环境配置并生成makefile
./build.FluffOS

# 编译并安装驱动文件到 `bin` 目录
make -j$(nproc) install

# 记录结束时间
endtime=`date +'%Y-%m-%d %H:%M:%S'`

# 计算编译时间
start_seconds=$(date --date=" $starttime" +%s);
end_seconds=$(date --date="$endtime" +%s);

# 输出编译时间
echo Start: $starttime.
echo End: $endtime.
echo "Build Time: "$((end_seconds-start_seconds))"s."
